package renta;

public interface Vehiculo {
	
	public String getModelo();
	public void setModelo(String Modelo);
	public String getTipo();
	public void setTipo(String Tipo);
	public int getCapacidad();
	public void setCapacidad(int capacidad);
	public int getKm();
	public void setKm(int Km);
	public String getEstado();
	public void setEstado(String Estado);
}
