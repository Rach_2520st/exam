package renta;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Arrendar {
	private ArrayList<Arrendatario> Usuarios = new ArrayList<Arrendatario>();
	private boolean InicioRenta;
	private int usuarios;
	private ArrayList<Vehiculo> Autos = new ArrayList<Vehiculo>();
	private int p;
	private String modelo;
	private String Tipo;
	private int Capacidad;
	private String Estado;
	private int opcion;
	public Arrendar() {
		System.out.println("Bienvenido a la reta de autos HERTZ");
		InicioRenta = false;
		
	}
	
	public void agregarauto() {
		Random rand = new Random();
		for(int i = 1; i<=10; i= i+1 ) {
			//genera un random para establecer si es un suv o es un auto
			//el random contiene dos numeros, 0 y 1 por eso se agrega el +1 al final
			p = rand.nextInt(2)+1;
			if (p==1) {
				Auto auto = new Auto();
				Tipo = "Auto";
				auto.setTipo(Tipo);
				//genera un random para establecer que tipo de modelo de auto se utilizara
				int m = rand.nextInt(2)+1;
				if(m==1) {
					modelo = "Mazda 2";
					Capacidad = 20;
					
				}
				else if(m==2) {
					modelo = "Kia Morning";
					Capacidad = 60;
				}
				auto.setModelo(modelo);
				auto.setCapacidad(Capacidad);
				int Km = rand.nextInt(2000)+1;
				auto.setKm(Km);
				Estado = "Desocupado";
				auto.setEstado(Estado);
				Autos.add(auto);
				}
			else if (p==2) {
				Suv suv = new Suv();
				Tipo = "Suv";
				suv.setTipo(Tipo);
				modelo = "Peugeot 2008";
				suv.setModelo(modelo);
				Capacidad = 40;
				suv.setCapacidad(Capacidad);
				int Km = (int)(Math.random()*2000);
				suv.setKm(Km);
				Estado = "Desocupado";
				suv.setEstado(Estado);
				Autos.add(suv);
				
			}
		
		}
		
		
		}
	public void mostrarOpciones(){
		
		System.out.println("largo del array: " + Autos.size());
		for (Vehiculo auto: Autos) {
			if(auto.getEstado().equals("Desocupado")) {
				
				System.out.println("el vehiculo es un: " + auto.getTipo());
				System.out.println("su modelo es: " +auto.getModelo());
				System.out.println("Tiene una capacidad de: " + auto.getCapacidad());
				System.out.println("Con un kilometraje de: " + auto.getKm());
		
			}
		}
		System.out.println("Ingresa que opción quieres escoger:");
		opcion = sc.nextInt();
		System.out.println("El vehiculo escogido es:"+ Autos.get(opcion).getTipo());
		System.out.println("modelo:" + Autos.get(opcion).getModelo());
		Estado = "Ocupado";
		Autos.get(opcion).setEstado(Estado);
	}
	
	public void IniciarRenta() {
		// No hay Usuarios, no puede empezar
		if(Usuarios.size() == 0){
			//System.out.printf("No existen usuarios");	
			usuarios = 0;
		}
		
		// Es como si ya se hubiese iniciado
		if(InicioRenta==true){
			return;
		}

		usuarios = 0;
		InicioRenta = true;
		// Agregar user
		agregarUsuario();
		// mostrar user
		exponerArrendatarios(Usuarios);
		
	}
	
	Scanner sc = new Scanner(System.in);
	public void agregarUsuario() {
		if(InicioRenta==true){
			String tipop;
			
			
			//pregunta y recibe que tipo de usuario se está registrando
			System.out.println("Tipo(El usuario debe indicar si es una persona o empresa):");
			tipop = sc.nextLine();
			//dependiendo de si es una persona o empresa utiliza diferentes clases hijas para permitir el acceso de datos
			if(tipop.equals("persona")) {
				Persona user = new Persona();
				System.out.println();
				System.out.println("----------Datos del Usuario -------------------------------");
				System.out.println("Nombre:");
				user.setNombreUsuario(sc.nextLine());
				System.out.println("Direccion:");
				user.setDireccion(sc.nextLine());
				System.out.println("Rut: ");
				user.setRut(sc.nextLine());
				user.settipop(tipop);
				Usuarios.add(user);
				}
			else if (tipop.equals("empresa")) {
				Empresa us = new Empresa();
				System.out.println();
				System.out.println("----------Datos del Usuario -------------------------------");
				System.out.println("Nombre:");
				us.setNombreUsuario(sc.nextLine());
				System.out.println("Direccion:");
				us.setDireccion(sc.nextLine());
				System.out.println("Rut: ");
				us.setRut(sc.nextLine());
				us.settipop(tipop);
				
				Usuarios.add(us);
			}
		
			System.out.println();
			
		}

	}
	public void exponerArrendatarios(ArrayList<Arrendatario> Usuarios){
		System.out.println("\n««««««««««««««« Usuario Registrado »»»»»»»»»»»»»»»\n");
		
		for (Arrendatario usr: Usuarios) {
			
			System.out.println("Identificacion:		" + usr.getNombreUsuario());
			System.out.println("Rut:			    " + usr.getRut());
			System.out.println("Direccion		    " + usr.getDireccion());
			System.out.println("---------------------------------------------");
			System.out.println("");
		}
	}
	public void actafinal() {
		System.out.print("Usted "+ Usuarios.get(0).getNombreUsuario());
		System.out.print(" ha arrendado el automovil marca: "+ Autos.get(opcion).getModelo());
		System.out.print(" con capacidad de combustible de: "+ Autos.get(opcion).getCapacidad());
		System.out.print(" con Kilometraje de" + Autos.get(opcion).getKm());
		System.out.print(" por favor, al termino de la jornada devolver/Estacionar el vehiculo en la posicion: " + opcion);
	}
}


