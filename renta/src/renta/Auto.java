package renta;

public class Auto implements Vehiculo{
	

	private String Modelo;
	private String Tipo;
	private int capacidad;
	private int Km;
	private String Estado;
	
	public Auto() {
		
	}
	public String getModelo() {
		return Modelo;
	}

	public void setModelo(String Modelo) {
		this.Modelo = Modelo;
		
	}

	public String getTipo() {
		return Tipo;
	}

	public void setTipo(String Tipo) {
		this.Tipo = Tipo;
	
		
	}


	public int getCapacidad() {
		
		return capacidad;
	}

	public void setCapacidad(int capacidad) {
		this.capacidad = capacidad;
		
	}

	
	public int getKm() {
	
		return Km;
	}

	public void setKm(int Km) {
		this.Km = Km;
		
	}
	
	public String getEstado() {
		
		return Estado;
	}

	public void setEstado(String Estado) {
		this.Estado = Estado;
		
	}

	
}	